
[本项目的Git仓库]: https://gitee.com/guobinyong/clocker



目录
=======================
```
一.简介
二.安装方式
三、正计时器的使用方式
四、倒计时器的使用方式
五、计时状态
六、接口文档
```


内容
================





# 一、简介
clocker-js 是计时器，有 正计时 和 倒计时 的功能，并且支持计时超时；

**如果您在使用该库的过程中有遇到了问题，或者有好的建议和想法，您都可以通过以下方式联系我，期待与您的交流：**  
- 邮箱：guobinyong@qq.com
- QQ：guobinyong@qq.com
- 微信：keyanzhe


# 二、安装方式
目前，安装方式有以下几种：


## 方式1：通过 npm 安装
```
npm install --save clocker-js
```

## 方式2：直接下载原代码
您可直接从 [本项目的Git仓库][] 下载，此仓库里包含了 clocker 和 下文的示例代码；clocker 库是 [本项目的Git仓库][] 项目中的 clocker/Clocker.js 文件，您可以直接把该文件拷贝到您的项目中去；然后使用如下代码在您的项目中引入 `Clocker`：
```
import { Clocker } from "path/to/package/Clocker.js";
```
或者
```
import Clocker from "path/to/package/Clocker.js";
```



# 三、正计时器的使用方式
1. 创建 目标时间 对象:
   ```
   let targetDate = new Date();
   ```
2. 用目标时间对象初始化计时器实例：
   ```
   let clocker = new Clocker(targetDate);  // 创建 计时器对象
   ```
   也可以先创建定时器，再更改 目标时间，如下：
   ```
   let clocker = new Clocker();  // 创建 计时器对象
   clocker.targetDate = targetDate;  // 更改 目标时间
   ```

3. 在你需要的时候获取定时器实例的相关计时信息；
   ```
   let year = clocker.year;
   let month = clocker.month;
   let date = clocker.date;
   let hours = clocker.hours;
   let minutes = clocker.minutes;
   let seconds = clocker.seconds;
   
   console.log(year,month,date,hours,minutes,seconds);
   ```




# 四、倒计时器的使用方式
1. 创建 目标时间 对象:
   ```
   let targetDate = new Date(2020,2,3,13,0,0);
   ```
2. 用目标时间对象初始化计时器实例：
   ```
   let clocker = new Clocker(targetDate,true);  // 创建 计时器对象
   ```
   也可以先创建定时器，再更改 目标时间，如下：
   ```
   let clocker = new Clocker();  // 创建 计时器对象;
   clocker.targetDate = targetDate;  // 更改 目标时间
   clocker.countDown = true;  // 设置 计时器 为倒计时器；
   ```

3. 在你需要的时候获取定时器实例的相关计时信息；
   ```
   let year = clocker.year;
   let month = clocker.month;
   let date = clocker.date;
   let hours = clocker.hours;
   let minutes = clocker.minutes;
   let seconds = clocker.seconds;
   
   console.log(year,month,date,hours,minutes,seconds);
   ```





# 五、计时状态
计时器实例有个只读属性 isCounting ，表示计时器的状态；  

当 isCounting 的值为 `true` 时，表示目前正在正常地计时；
当 isCounting 的值为 `false` 时，表示目处于非正在正常计时状态，具体的含义如下：
- 在正计时中表示：还未到达开始计时时间；
- 在倒计时中表示：已经到达结束时间；

并且，当 isCounting 的值为 `false` 时，实例的 year、month、date、hours、minutes、seconds、milliseconds 均是负值；






# 六、接口文档

## constructor
`constructor(targetDate, countDown)`
- @param targetDate : Date     目标时间，在正计时中，目标时间是计时的起始时间；在倒计时中，目标时间为结束时间；默认为当前时间
- @param countDown : boolean      是否是倒计时，默认为 false



## isCounting

- @readonly : boolean

**说明：**  
计时是否正在正常地进行中

**注意：**  
- 在正计时中，返回 `false` 表示：还未到达开始计时时间；
- 在倒计时中，返回 `false` 表示：已经到达结束时间；




## dateObj
- @readonly  : Date

**说明：**  
获得计时的 Date 对象 (以世界标准时间(UTC)计时)



## year
- @readonly : number

**说明：**  
获得计时的年数
 
  

## month
- @readonly : number

**说明：**  
获得计时的月数
 

## date
- @readonly : number

**说明：**  
获得计时的天数



## hours
@readonly : number

**说明：**  
获得计时的小时数

 
## minutes
- @readonly : number

**说明：**  
获得计时的分钟数


## seconds
- @readonly : number

**说明：**  
获得计时的秒钟数
 


## milliseconds
- @readonly : number

**说明：**  
获得计时的毫秒数